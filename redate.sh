# !/bin/zsh
# Script para generar y preparar los archivos para grabar en un cd
# Hay que cambiar la variable de TARGETTIME a la fecha deseada
# Solo se cambia la fecha de los archivos más nuevos que la fecha asignada
TARGETTIME="2018-08-20 16:45:12"
PLATFORM='unknown'

# En caso de querer construir el proyecto
function rebuild_proyect() {
  yarn
  yarn generate
  git archive --format=zip --worktree-attributes -o archive.zip HEAD
  zip -ur archive.zip dist
}

function date_files() {
  mkdir -p _archive
  unzip archive.zip _archive

  if [[ $OSTYPE in darwin* ]]; then
    PLATFORM='DARWIN'
    echo "Esto es una mac, usare el prefix 'g' para touch y find \n"
    echo "Necesitas instalar com brew lo siguiente:\n brew install libfaketime findutils coreutils"
    gfind _archive -newermt $TARGETTIME -exec gtouch -a -m -d $TARGETTIME {} \;
    gfind _archive -newerat $TARGETTIME -exec gtouch -a -m -d $TARGETTIME {} \;
    open _archive
  elif [[ $OSTYPE in linux* ]]; then
    PLATFORM='LINUX'
    echo "Esto es linux, no usaré el prefix para touch o find"
    find _archive -newermt $TARGETTIME -exec touch -a -m -d $TARGETTIME {} \;
    find _archive -newerat $TARGETTIME -exec touch -a -m -d $TARGETTIME {} \;
    xdg-open _archive &
  fi
}

read -q "REBUILD?Quires compilar y comprimir el proyecto? [y/N]"
case "$REBUILD" in
  [yY][eE][sS]|[yY])
      rebuild_proyect()
      ;;
  *)
      echo "\nNo se reconstruye ni se comprime el proyecto"
      ;;
esac

read -q "PROCESSFILES?Quieres cambiar la fecha de los archivos a $TARGETTIME"

case "$PROCESSFILES" in
  [yY][eE][sS]|[yY])
      date_files()
      ;;
  *)
      echo "\nNo se cambió la fecha de los archivos"
      ;;
esac

echo "Script terminado"




