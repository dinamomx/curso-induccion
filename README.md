> Plantilla Nuxt para Dinamo

[TOC]

# Setup

```bash
# Instala lo que este configurado en el repositorio y branch seleccionado.
yarn

# Inicia el proyecto en modo de desarrollo y actualiza el navegador
yarn serve

# Equivalente a yarn run build genera los archivos necesarios para publicar en el FTP
yarn build
```

# Comandos disponibles

```bash
# Revisa errores en el código (sass, js y vue) e intenta corregirlos
yarn lint
```

# Deploy

Hay una serie de scripts preparados para "deploy" el proyecto. Dependiendo de la forma que se desee usar, está el uso con node js y html estático.

Hay que configurar los archivos correspondientes `ecosystem.conf.js`, `.env.production`, `uploadbuild.js` o `subircambios`

```bash
# Para subir los archivos estáticos
yarn run deploy:static
# Para por primera vez subir el proyecto con nodejs
yarn run pm2 deploy production setup
yarn run pm2 deploy staging setup
# Para actualizar el proyecto con nodejs
yarn run deploy:staging
yarn run deploy:production
```

# Linter

Estamos usando un verificiador de estilo y errores automático para ayudarnos tanto a mantener un código consistente como fácil de mantener y con la menor cantidad de errores posible.

El estilo de código que usamos y por consiguiente las reglas que se siguen son una mezcla de [airbnb/base](https://github.com/airbnb/javascript#table-of-contents) y [vue/recomended](https://vuejs.org/v2/style-guide/). Airbnb funciona como base y vue como complemento. En los enlaces están las referencias a las reglas que se usan con ejemplo de uso y razonamiento detrás de la regla. En el caso de las reglas propietarias de vue [esta es la referencia completa](https://github.com/vuejs/eslint-plugin-vue#bulb-rules) ya que sigue en desarrollo.

Es muy recomendado instalar los complementos de eslint para el editor ya que estos te explican en que consiste un error y así corregirlo conforme se escribe.

#### Atom

- [linter-eslint](https://atom.io/packages/linter-eslint)
- [linter](https://atom.io/packages/linter)
- [linter-ui-default](https://atom.io/packages/linter-ui-default)
- [language-vue](https://atom.io/packages/language-vue)

Configuración recomendada para ver los errores en la sección `<template>`:

```cson
  linter: {}
  "linter-eslint":
    fixOnSave: true
    lintHtmlFiles: true
    scopes: [
      "source.js"
      "source.jsx"
      "source.js.jsx"
      "source.babel"
      "source.js-semantic"
      "source.vue"
      "text.html.vue"
    ]
```

# VScode

Se puede usar el plugin [Settings Sync](https://marketplace.visualstudio.com/items?itemName=Shan.code-settings-sync) para copiar la misma configuración de divo que ya incluye todo lo necesario para trabajar con los linters y demás goodies.

El gist es `705605944736f710701ffe1eba2804ac` y el token es `a14524d58de7d05199534af09582cf3e78812e3e`

- Primero haz click en el botón de "Extensions" a la derecha
- Después busca "Sync" y selecciona "Settings Sync" e instalalo
- Después de instalarlo tienes que hacer click en "reload"
- Una vez recargado vscode presiona `cmd+shift+p` para abrir el panel de comandos, busca y ejecuta "Sync: Download Settings". Esto te pedira la id del gist y el token de acceso a github (puestos aquí arriba)
- Espera unos minutos en lo que se instalan los plugins y recarga vscode
- Listo!

# Formualrios

Las instrucciones para los campos de formularios y ejemplos relevantes se encuentra en la documentación de [Buefy.js](https://buefy.github.io/#/documentation/field)

# Meta y SEO
