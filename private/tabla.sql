SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `trivias`;
CREATE TABLE `trivias` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `trivias` VALUES(1, '1. La igualdad: principio constitucional y derecho humano', 'igualdad/');
INSERT INTO `trivias` VALUES(2, '2. ¿Por qué actuar a favor de la igualdad entre mujeres y hombres? ', 'porque/');
INSERT INTO `trivias` VALUES(3, '3. Principales obligaciones del gobierno mexicano\nen materia de igualdad', 'principales-obligaciones/');
INSERT INTO `trivias` VALUES(4, '4. Mi relevante papel en la construcción de la igualdad entre mujeres y hombres', 'mi-papel/');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creation_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_modification_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `moodle_id` int(11) NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `user_trivias`;
CREATE TABLE `user_trivias` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `trivia_id` int(11) UNSIGNED NOT NULL,
  `completed` tinyint(1) UNSIGNED DEFAULT 0,
  `latest_score` int(11) UNSIGNED DEFAULT 0,
  `tries` int(11) UNSIGNED DEFAULT 0,
  `past_scores` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


ALTER TABLE `trivias`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `moodle_id` (`moodle_id`);

ALTER TABLE `user_trivias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_trivias_user` (`user_id`),
  ADD KEY `users_trivias_trivias` (`trivia_id`);


ALTER TABLE `trivias`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `user_trivias`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;


ALTER TABLE `user_trivias`
  ADD CONSTRAINT `user_trivias_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_trivias_trivias` FOREIGN KEY (`trivia_id`) REFERENCES `trivias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
