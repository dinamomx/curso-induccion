<?php
/**
 * PHP-CRUD-API v2              License: MIT
 * Maurits van der Schee: maurits@vdschee.nl
 * https://github.com/mevdschee/php-crud-api
 **/

namespace Tqdev\PhpCrudApi;

require_once('./_src.php');
// file: src/index.php

$config = new Config([
    'driver'      => 'mysql', // defaults
    'address'     => 'mysql.induccion-inmujeres.wdinamo.com',
    'port'        => null, // defaults
    'username'    => 'animales',
    'password'    => 'gato666_',
    'database'    => 'sfa_induccion',
    'middlewares' => 'cors',
    'controllers' => 'records,openapi',
    'cacheType'   => 'TempFile',
    'cachePath'   => './',
    'cacheTime'   => 600,
    'debug'       => false,
    'openApiBase' => '{"info":{"title":"Inducción-INMUJERES","version":"1.0.0"}}',
    // Limitaciones de dominios
    'cors.allowedOrigins'   => '*',
    'cors.maxAge'   => '60',
    'cors.allowHeaders'  => 'Authorization,X-Requested-With,Content-Type,X-Authorization, X-XSRF-TOKEN',
    'cors.allowCredentials' => 'true',
]);

$request = new Request();
$api = new Api($config);
$response = $api->handle($request);
$response->output();
