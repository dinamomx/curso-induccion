/* eslint-disable */

(function cursoConfig() {
  const config = {
    // Mostrar siempre el curso o solo cuando se cumpla el referer y el user_id
    ALWAYS_SHOW_APP: true,
    // Esto ayuda a bloquear la entrada de usuarios válidos de moodle
    REFERER_DOMAIN: "campusgenero.inmujeres.gob.mx",
    // Como se llama el parámetro en la url que contiene la id del usuario
    USER_ID_PARAM: 'user_id',
    // La url de la api
    API_URL: 'http://induccion-inmujeres.wdinamo.com/api',
    // El usuario del basic Auth de la api (si está habilitdado)
    API_USER: 'admin',
    // El password del basic Auth de la api (si está habilitdado)
    API_PASSWORD: 'password',
    // Iframes de MOODLE
    MOODLE_IFRAMES: {
      inicio: {
        0: 'http://puntogenero.inmujeres.gob.mx'
      },
      cierre: {
        0: 'http://puntogenero.inmujeres.gob.mx/moodle_puntogenero/mod/quiz/view.php?id=2557',
        1: 'http://puntogenero.inmujeres.gob.mx/moodle_puntogenero/mod/quiz/view.php?id=2557',
        2: 'http://puntogenero.inmujeres.gob.mx/moodle_puntogenero/mod/quiz/view.php?id=2557',
        3: 'http://puntogenero.inmujeres.gob.mx/moodle_puntogenero/mod/quiz/view.php?id=2557',
        4: 'http://puntogenero.inmujeres.gob.mx/moodle_puntogenero/mod/quiz/view.php?id=2557',
      },
    },
  };

  window.__CursoConfig__ = config;
})();
