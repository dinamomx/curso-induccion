import Vue from 'vue'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import { library, config } from '@fortawesome/fontawesome-svg-core'
import {
  faChevronCircleRight,
  faChevronCircleLeft,
  faAngleDown,
  faAngleUp,
  faHandPointRight,
  faMapMarkerAlt,
  faChevronSquareLeft,
  faChevronSquareRight,
} from '@fortawesome/pro-solid-svg-icons'

config.showMissingIcons = true
config.measurePerformance = process.env.NODE_ENV === 'production'

library.add([
  faChevronCircleRight,
  faChevronCircleLeft,
  faAngleUp,
  faAngleDown,
  faHandPointRight,
  faMapMarkerAlt,
  faChevronSquareLeft,
  faChevronSquareRight,
])

Vue.component('fa-icon', FontAwesomeIcon)
