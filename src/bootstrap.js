async function getPolifylls() {
  const URLSearchParams = await import('@ungap/url-search-params')
  window.URLSearchParams = URLSearchParams.default || URLSearchParams
}

if (!('URLSearchParams' in window)) {
  getPolifylls()
}
