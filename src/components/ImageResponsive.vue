<template>
  <picture
    ref="parent"
    :class="[
      'image-responsive',
      {'image-responsive--failed': failed},
      {'image-responsive--loaded': loaded}
    ]"
    :style="[imageGradient]"
    class="image">
    <img
      v-if="responsive && isVisible"
      v-show="loaded"
      :src="normalSrc"
      :srcset="srcset"
      :sizes="sizes"
      class="image-responsive__img"
      @load="imageLoaded"
      @error="imageLoaded">
    <img
      v-else-if="isVisible"
      v-show="loaded"
      :src="baseUrl + src"
      class="image-responsive__img"
      @load="imageLoaded"
      @error="imageLoaded">
  </picture>
</template>

<script>
/**
 * Imagen Responsiva con carga demorada (LazyLoad)
 *
 * @version 1.1
 * @author: César Valadez | DinamoMX
 * @since: 2018-01-19
 * @desc: Imagen Responsiva, esto es un wrapper para el simple img
 * @see: https://www.smashingmagazine.com/2014/05/responsive-images-done-right-guide-picture-srcset/
 * @see: https://ericportis.com/posts/2014/srcset-sizes/
 */
export default {
  name: 'ImageResponsive',
  props: {
    /**
     * La url o path de la imagen
     *
     * @example '/placeholder/demo.jpg'
     */
    src: {
      type: String,
      required: true,
    },
    /**
     * El texto alternativo de la imagen
     */
    alt: {
      type: String,
      default: 'Placeholder, please fill me',
    },
    /**
     * Los Tamaños de la imagen de acuerdo al atributo nativo sizes
     * el ancho esperado de la imagen de acuerdo al ancho
     * de la pantalla.
     *
     * @example (min-width: 650px) 50vw, 100vw
     */
    sizes: {
      type: String,
      default: '(min-width: 769px) 90vw, 100vw',
    },
    /**
     * Los archivos de tamaño de imagen a buscar
     * corresponde al atributo nativo srcset
     *
     * @example
      [
        {separator: '_',word: 'thumb',size: '480w'},
        {separator: '_', word: 'medium', size: '720w'},
      ]
      * @see [Guía a srcset y sizes] https://ericportis.com/posts/2014/srcset-sizes/
     */
    imageSizes: {
      type: Array,
      default: () => [
        {
          separator: '_',
          word: 'thumb',
          size: '480w',
        },
        {
          separator: '_',
          word: 'medium',
          size: '720w',
        },
        {
          separator: '_',
          word: 'large',
          size: '1124w',
        },
        {
          separator: '_',
          word: 'full',
          size: '1780w',
        },
      ],
    },
    /**
     * IntersectionObserver threshold
     */
    threshold: {
      type: [Array, Number],
      default: () => [0, 0.5, 0.5],
    },
    /**
     * InserectionObserver visibility ratio
     */
    ratio: {
      type: Number,
      default: 0.4,
      validator(value) {
        // can't be less or equal to 0 and greater than 1
        return value > 0 && value <= 1
      },
    },
    /**
     * IntersectionObserver root margin
     */
    margin: {
      type: String,
      default: '0px',
    },
    /**
     * Los colores del gradiente mientras carga la imagen
     */
    colors: {
      type: Array,
      default: () => ['#F83F28', '#472533', '#211F35'],
    },
    /**
     * Hacer o no responsiva la imagen
     */
    responsive: {
      type: Boolean,
      default: true,
    },
    /**
     * Url Base para las imagenes
     */
    baseUrl: {
      type: String,
      default: '/images',
    },
  },
  data: () => ({
    /**
     * Carga exitosa de la imagen
     * @type {Boolean}
     */
    loaded: false,
    /**
     * Fallo en la carga de la imagen
     * @type {Boolean}
     */
    failed: false,
    /**
     * La imagen es visible
     * @type {Boolean}
     */
    isVisible: false,
    /**
     * Estamos escuchando al IntersectionObserver
     * @type {Boolean}
     */
    listening: false,
    /**
     * La instancia de IntersectionObserver
     * @type {IntersectionObserver}
     */
    observer: null,
  }),
  computed: {
    /**
     * Separa la ruta, nombre del archivo y formato de la imagen
     *
     * @requires this.src
     * @returns {Object} image Partes de la ruta de la imagen
     * @returns {String} image.route La ruta de la ruta relativa del archivo
     * @returns {String} image.name El nombre del archivo sin la extensión
     * @returns {String} image.dormat La extensión del archivo
     */
    image() {
      const [, route, name, format] = this.src.match(
        /(\S+)?\/(\S*)(.jpg|png|svg)$/
      )
      return { route, name, format }
    },
    /**
     * Convierte el array de colores a una string css
     *
     * @returns {Object} style
     * @returns {Object} style.background-image CSS linear-gradient
     */
    imageGradient() {
      if (this.failed) return {}
      const colors = this.colors.join(', ')

      return {
        'background-image': `linear-gradient(35deg, ${colors})`,
      }
    },
    /**
     * La imagen de menor tamaño se usa en src
     *
     * @returns {String} La url más pequeña de la imagen
     */
    normalSrc() {
      const size = this.imageSizes[0]
      let result = this.baseUrl
      result += this.image.route
        ? `/${this.image.route}/${this.image.name}${size.separator}${
            size.word
          }${this.image.format}`
        : `/${this.image.name}${size.separator}${size.word}${this.image.format}`
      return result
    },
    /**
     * El valor para el atributo srcset
     *
     * @returns {String} El srcset
     * @see [Artículo MDN sobre las imagenes adaptables] https://developer.mozilla.org/es/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images
     */
    srcset() {
      const arrayOfSizes = []
      for (let i = 0; i < this.imageSizes.length; i++) {
        const size = this.imageSizes[i]
        let result = this.baseUrl
        result += this.image.route
          ? `/${this.image.route}/${this.image.name}${size.separator}${
              size.word
            }${this.image.format} ${size.size}`
          : `/${this.image.name}${size.separator}${size.word}${
              this.image.format
            } ${size.size}`
        arrayOfSizes.push(result)
      }
      return arrayOfSizes.join(', ')
    },
  },
  mounted() {
    this.$nextTick(this.observe)
  },
  methods: {
    /**
     * Crea una instancia de IntersectionObserver y observa esta imagen.
     */
    observe() {
      const elementToObserve = this.$refs.parent || this.$el
      if (!elementToObserve) {
        return
      }
      const options = {
        threshold: this.threshold,
        root: this.element ? document.querySelector(this.element) : null,
        rootMargin: this.margin,
      }

      // creates IO instance
      this.observer = new IntersectionObserver(entries => {
        // as we instantiated one for each component
        // we can directly access the first index
        if (entries[0].intersectionRatio >= this.ratio) {
          this.isVisible = true
        }
      }, options)
      // start observing main component
      this.observer.observe(elementToObserve)
    },
    /**
     * Es llamado cuando la imagen se carga o falla
     *
     * @param {SyntheticEvent} event El evento de la carga
     */
    imageLoaded(event) {
      this.observer = null
      if (event.type === 'error') {
        this.failed = true
      } else {
        this.loaded = true
      }
    },
  },
}
</script>

<style lang="scss">
/* @define image-responsive */

.image-responsive {
  position: relative;
  min-width: 100%;
  max-width: 100%;
  overflow: hidden;

  &__img {
    width: 100%;
    height: 100%;
    opacity: 0;
    object-fit: cover;
    animation: fade-in 300ms linear forwards;
    animation-delay: 300ms;
  }
  // HACK: para evitar ser jodidos por bulma
  // stylelint-disable-next-line
  img.image-responsive__img {
    width: 100%;
    height: 100%;
  }

  &--failed {
    min-width: 200px;
    min-height: 200px;
    background-color: #be3730;
    background-image: url('@/assets/broken.png');
    background-repeat: no-repeat;
    background-position: center;
  }

  &--loaded {
    background: none;
  }
}

@keyframes fade-in {
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
}
</style>

<docs>
Componente para usar imagenes responsivas (srcset + sizes) con lazy load

## Examples

Imagen básica usando las opciones por defecto

```jsx
<image-responsive
  src="placeholder/example.jpg"
  alt="Imagen de desmostración"/>
```

Imagen remota

```jsx
<image-responsive
  src="/images/placeholder/example.jpg"
  baseUrl="https://plantilla.dinamo.mx"
  alt="Imagen de desmostración"/>
```

</docs>
