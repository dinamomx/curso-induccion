import {
  getUserById,
  postUserData,
  userDataSchema,
  putUserData,
} from '@/api/users'

import { SET_USER_ID, SET_USER_DATA } from './types'

export const state = () => {
  const USER_ID_PARAM = 'user_id'
  const params = new URLSearchParams(document.location.search)
  let userFromParams = null
  if (params.has(USER_ID_PARAM)) {
    userFromParams = parseInt(params.get(USER_ID_PARAM), 10)
  }
  return {
    userID: userFromParams,
    data: {},
  }
}

export const mutations = {
  [SET_USER_ID](st, userID) {
    st.userID = userID
  },
  [SET_USER_DATA](st, userDataSchemaObject = userDataSchema) {
    st.data = userDataSchemaObject
  },
}

export const getters = {
  isLogged(st) {
    return !!st.userID && !!st.data.moodle_id
  },
}

/** @type {import("vuex").ActionTree<typeof state>} */
export const actions = {
  async getUserDataFromApi({ commit, dispatch, state: st }, userID) {
    // eslint-disable-next-line no-param-reassign
    userID = userID || st.userID
    // Obtiene los datos desde la api
    try {
      const userDataFromApi = await getUserById(userID)
      const userDataSchemaObject = Object.assign(
        {},
        userDataSchema,
        userDataFromApi
      )

      commit(SET_USER_DATA, Object.assign({}, userDataSchemaObject))
    } catch (error) {
      if (!error.response) {
        // eslint-disable-next-line no-console
        console.error('Error de red, no se encontró la api', error)
      } else if (error.response.status === 404) {
        // Si no lo encontró lo creo
        await dispatch('setUserData', userID)
      } else {
        // eslint-disable-next-line no-console
        console.error({ error })
      }
    }
    // Asegura que el schema del usuario sea el esperado
  },
  /**
   * Crea un nuevo usuario, solo debe llamarse del mismo store
   * @param {VuexStore} store
   * @param {Number} userID La id del usuario
   */
  async setUserData({ commit }, userID) {
    try {
      const userDataFromApi = await postUserData(userID)
      const userDataSchemaObject = Object.assign(
        {},
        userDataSchema,
        userDataFromApi
      )
      commit(SET_USER_DATA, userDataSchemaObject)
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(error)
    }
  },

  async updateUserData({ dispatch }, userData) {
    try {
      await putUserData(userData)
      await dispatch('getUserDataFromApi', userData.moodle_id)
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(error)
    }
  },
}
