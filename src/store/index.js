import Vue from 'vue'
import Vuex from 'vuex'
import * as user from './user'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    // eslint-disable-next-line no-underscore-dangle
    config: window.__CursoConfig__ || {},
  },
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    user: {
      ...user,
      namespaced: true,
    },
  },
})

export default store
