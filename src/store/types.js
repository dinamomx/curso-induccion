export const META_CHANGED = 'META_CHANGED'
export const SET_SLIDE = 'SET_SLIDE'
export const SET_USER_ID = 'SET_USER_ID'
export const SET_USER_DATA = 'SET_USER_DATA'
