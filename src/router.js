import Vue from 'vue'
import Router from 'vue-router'
import Index from './views/index.vue'

Vue.use(Router)

const routes = [
  {
    path: '*',
    name: 'Missing',
    component: () => import('./views/NotFound.vue'),
  },
  {
    path: '',
    name: 'index',
    component: Index,
  },
  {
    path: '/cierre',
    name: 'cierre',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "cierre" */ './views/cierre.vue'),
  },
  {
    path: '/desigualdades',
    name: 'desigualdades',
    component: () =>
      import(/* webpackChunkName: "desigualdades" */
      './views/desigualdades.vue'),
  },
  {
    path: '/guia-del-curso',
    name: 'guia-del-curso',
    component: () =>
      import(/* webpackChunkName: "guia-del-curso" */
      './views/guia-del-curso.vue'),
  },
  {
    path: '/igualdad',
    name: 'igualdad',
    component: () =>
      import(/* webpackChunkName: "igualdad" */ './views/igualdad.vue'),
  },
  {
    path: '/inicio',
    name: 'inicio',
    component: () =>
      import(/* webpackChunkName: "inicio" */ './views/inicio.vue'),
  },
  {
    path: '/mi-papel',
    name: 'mi-papel',
    component: () =>
      import(/* webpackChunkName: "mi-papel" */ './views/mi-papel.vue'),
  },
  {
    path: '/obligaciones',
    name: 'obligaciones',
    component: () =>
      import(/* webpackChunkName: "obligaciones" */
      './views/obligaciones.vue'),
  },
]

const router = new Router({
  linkActiveClass: 'is-active',
  mode: 'hash',
  linkExactActiveClass: 'is-active--exact',
  /**
   * Un Comportamiento de scroll donde automáticamente hace scroll
   * a la ruta hija más profunda posible.
   *
   * @param {VueRoute} to - De donde venimos.
   * @param {VueRoute} from - A Donde vamos.
   * @param {number} savedPosition - La posición previa (back).
   *
   * @returns {{Promise}} La posición en la pantalla.
   */
  scrollBehavior: (to, from, savedPosition) => {
    // if the returned position is falsy or an empty object,
    // will retain current scroll position.
    let position = false
    let scrollToChild = false

    // if no children detected
    if (to.matched.length < 2) {
      // scroll to the top of the page
      position = {
        x: 0,
        y: 0,
      }
    } else if (to.matched.some(r => r.components.default.options.scrollToTop)) {
      // if one of the children has scrollToTop option set to true
      position = {
        x: 0,
        y: 0,
      }
    } else {
      scrollToChild = true
    }

    // savedPosition is only available for popstate navigations (back button)
    if (savedPosition) {
      position = savedPosition
    }

    return new Promise(resolve => {
      // wait for the out transition to complete (if necessary)
      window.$app.$once('triggerScroll', () => {
        // coords will be used if no selector is provided,
        // or if the selector didn't match any element.
        if (to.hash && document.querySelector(to.hash)) {
          // scroll to anchor by returning the selector
          position = {
            selector: to.hash,
          }
        } else if (scrollToChild) {
          // Always scroll to child components if there is one
          const lastMatch = [...to.matched].pop()
          const lastChild = lastMatch.instances.default.$el
          const { top } = lastChild.getBoundingClientRect()
          position = {
            x: 0,
            y: Math.max(0, top + window.scrollY - 100),
          }
        }
        resolve(position)
      })
    })
  },

  routes,
})

export default router
