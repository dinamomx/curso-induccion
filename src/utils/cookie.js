const Cookie = {
  tld: false, // if true, set cookie domain at top level domain
  /**
   * @description Sets the cookie
   *
   * @param {string} name The name of the cookie
   * @param {string} value The value of the cookie
   * @param {number} days How long the cookie should exist
   * @returns {string} The cookie
   */
  set(name, value, days) {
    const cookie = { [name]: value, path: '/' }

    if (days) {
      const date = new Date()
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000)
      cookie.expires = date.toUTCString()
    }

    if (Cookie.tld) {
      cookie.domain = `.${window.location.hostname
        .split('.')
        .slice(-2)
        .join('.')}`
    }

    const arr = []
    Object.keys(cookie).forEach(key => {
      arr.push(`${key}=${cookie[key]}`)
    })
    document.cookie = arr.join('; ')

    return this.get(name)
  },

  /**
   * @description Gets all the cookies
   *
   * @returns {Object<string, string>} The cookies
   */
  getAll() {
    const cookie = {}
    document.cookie.split(';').forEach(el => {
      const [k, v] = el.split('=')
      cookie[k.trim()] = v
    })
    return cookie
  },

  /**
   * @description Gets one cookie
   *
   * @param {string} name The name of the cookie to get
   * @returns {string} The cookie value
   */
  get(name) {
    return this.getAll()[name]
  },

  /**
   * @description Borra una cookie
   *
   * @param {string} name El nombre de la cookie a borrar
   */
  delete(name) {
    this.set(name, '', -1)
  },
}

export default Cookie
