/* eslint-disable no-param-reassign */
// import JsPdf from 'jspdf'
import html2canvas from 'html2canvas'

/**
 * Convierte un div a pdf
 * @param {Element} htmlEl
 * @param {Boolean} toPDF
 * @param {boolean} newWindow
 */
export function toDataUrl(htmlEl) {
  /** @type {Element} */
  const copyDom = htmlEl.cloneNode(true)
  copyDom.classList.remove('is-hidden')
  copyDom.removeAttribute('hidden')
  copyDom.setAttribute(
    'style',
    'position:absolute; left: -5000px; top: -500px;'
  )
  document.body.appendChild(copyDom)
  return html2canvas(copyDom)
    .then(canvas => {
      const dataUrl = canvas.toDataURL('image/jpeg', 1.0)
      copyDom.remove()
      return dataUrl
    })
    .catch(e => {
      // eslint-disable-next-line no-console
      console.error(e)
      throw e
    })
}

/**
 * Guarda como pdf
 * @param {string} dataUrl DataUri
 * @param {string} filename FileName
 */
export function toPdf(dataUrl, filename) {
  filename = filename || 'descarga.pdf'
  // eslint-disable-next-line
  const pdf = new JsPdf()
  pdf.addImage(dataUrl, 'JPEG', 0, 0)
  pdf.save(filename)
}
/**
 * Guarda el archivo
 * @param {string} uri Data Uri
 * @param {String} filename
 */
export function saveAsImage(uri, filename) {
  const link = document.createElement('a')

  if (typeof link.download === 'string') {
    link.href = uri
    link.download = filename

    // Firefox requires the link to be in the body
    document.body.appendChild(link)

    // simulate click
    link.click()

    // remove the link when done
    document.body.removeChild(link)
  } else {
    window.open(uri)
  }
}
