/* eslint-disable no-restricted-syntax */

export default function transformApi(tables) {
  const tree = {}
  const arrayFlip = trans => {
    let key

    const tmpArr = {}
    for (key in trans) {
      if (Object.prototype.hasOwnProperty.call(trans, key)) {
        tmpArr[trans[key]] = key
      }
    }
    return tmpArr
  }
  const getObjects = (localTable, tableName, whereIndex, matchValue) => {
    const objects = []
    for (let record in localTable[tableName].records) {
      if (
        Object.prototype.hasOwnProperty.call(
          localTable[tableName].records,
          record
        )
      ) {
        record = localTable[tableName].records[record]
        if (!whereIndex || record[whereIndex] === matchValue) {
          const object = {}
          for (const index in localTable[tableName].columns) {
            if ({}.hasOwnProperty.call(localTable[tableName].columns, index)) {
              const column = localTable[tableName].columns[index]
              object[column] = record[index]
              for (const relation in localTable) {
                if ({}.hasOwnProperty.call(localTable, relation)) {
                  const reltable = localTable[relation]
                  for (const key in reltable.relations) {
                    if ({}.hasOwnProperty.call(reltable.relations, key)) {
                      const target = reltable.relations[key]
                      if (target === `${tableName}.${column}`) {
                        const columnIndices = arrayFlip(reltable.columns)
                        object[relation] = getObjects(
                          localTable,
                          relation,
                          columnIndices[key],
                          record[index]
                        )
                      }
                    }
                  }
                }
              }
            }
          }
          objects.push(object)
        }
      }
    }
    return objects
  }
  for (const name in tables) {
    if ({}.hasOwnProperty.call(tables, name)) {
      const table = tables[name]
      if (!table.relations) {
        tree[name] = getObjects(tables, name)
        if (table.results) {
          // eslint-disable-next-line
          tree._results = table.results
        }
      }
    }
  }
  return tree
}
