import Axios from 'axios'
import Cookie from '@/utils/cookie'
/* eslint-disable no-console */

export function UserNotFoundException(message, response) {
  this.message = message || 'Usuario no encontrado'
  this.name = 'UserNotFoundException'
  this.response = response
  this.stack = new Error().stack
}

/**
 * @typedef {Object} TriviaSchema
 * @property {boolean} completed
 * @property {number} tries
 * @property {number} triviaId
 * @property {number[]} pastScores
 * @property {number} latestScore
 * @property {string} name
 * @property {string} url
 */

/** @type {TriviaSchema} */
export const triviaSchema = {
  completed: false,
  tries: 0,
  triviaId: null,
  pastScores: [],
  latestScore: null,
  name: '',
  url: '',
}

/**
 *
 * @param {Object} triviaFromApi
 * @returns {TriviaSchema}
 */
const formatTriviasData = triviaFromApi => {
  return {
    ...triviaSchema,
    id: triviaFromApi.id,
    tries: triviaFromApi.tries,
    triviaId: triviaFromApi.trivia_id,
    pastScores: JSON.parse(triviaFromApi.past_scores || '[]'),
    latestScore: triviaFromApi.latest_score,
    name: triviaFromApi.name,
    url: triviaFromApi.url,
  }
}
/**
 *
 * @param {Object} triviaFromApp
 * @returns {TriviaSchema}
 */
const unformatTriviasData = triviaFromApp => {
  return {
    id: triviaFromApp.id,
    tries: triviaFromApp.tries,
    user_id: triviaFromApp.userId,
    trivia_id: triviaFromApp.triviaId,
    past_scores: JSON.stringify(triviaFromApp.pastScores || []),
    latest_score: triviaFromApp.latestScore,
    name: triviaFromApp.name,
    url: triviaFromApp.url,
  }
}
/** @param {Date} date */
const toMysqlDate = date =>
  new Date(date)
    .toISOString()
    .slice(0, 19)
    .replace('T', ' ')

/**
 * @typedef {Object} userDataSchema
 * @param {string} username
 * @param {array} trivias
 * @param {number} id
 * @param {string|Date} creation_date
 * @param {string|Date} last_modification_date
 * @param {string} first_name
 * @param {string} last_name
 * @param {number} moodle_id
 * @param {string|object} metadata
 */

/**
 * @type {userDataSchema}
 */
export const userDataSchema = {
  username: 'Nombre del usuario',
  trivias: [],
  creation_date: new Date(),
  last_modification_date: new Date(),
  first_name: null,
  last_name: null,
  moodle_id: null,
  metadata: '',
}

// eslint-disable-next-line no-underscore-dangle
const { API_TOKEN, API_URL } = window.__CursoConfig__

export const api = Axios.create({
  baseURL: `${API_URL}/records`,
  withCredentials: true,
  responseType: 'json',
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'X-Authorization': API_TOKEN,
    // Authorization: `${API_USER}:${API_PASSWORD}`,
  },
})

api.interceptors.request.use(config => {
  // config.headers['X-XSRF-TOKEN'] = Cookie.get('XSRF-TOKEN')
  return config
})

api.interceptors.response.use(response => {
  const token = response.headers['x-xsrf-token']
  if (token) {
    Cookie.set('XSRF-TOKEN', token)
  }
  return response
})

export const getUserById = async userId => {
  const { data } = await api.get(
    `users?filter=moodle_id,eq,${userId}&join=user_trivias`
  )
  const [user] = data.records
  if (!user) {
    throw new UserNotFoundException(`Usuario${userId} no encontrado`, {
      status: 404,
    })
  }
  // Transform User
  user.trivias = user.user_trivias.map(formatTriviasData)
  // console.log('formatUserData', { trivias: [...user.trivias] })
  delete user.user_trivias
  if (user.metadata) {
    user.metadata = JSON.parse(user.metadata)
  }
  Object.keys(user).forEach(key => {
    if (user[key] === 'null') {
      user[key] = null
    }
  })
  return user
}

/**
 *
 * @param {UserDatSchema} obj
 */
const normalizeUserData = obj => {
  const newObj = {}
  Object.keys(obj).forEach(key => {
    if (obj[key] instanceof Date) {
      newObj[key] = toMysqlDate(obj[key])
    } else if (typeof obj[key] === 'object') {
      newObj[key] = JSON.stringify(obj[key])
    } else {
      newObj[key] = obj[key]
    }
  })
  return newObj
}

export const postTrivia = triviaElement => {
  const trivia = unformatTriviasData(triviaElement)
  if (trivia.id) {
    return api.put(`user_trivias/${trivia.id}`, trivia)
  }
  if (trivia.trivia_id && trivia.user_id) {
    return api.post('user_trivias', trivia)
  }
  console.warn('Trivia incompleta', { triviaElement, trivia })
  return Promise.resolve()
}

/**
 * Crea un nuevo usuario
 * @param {int} userId ID del usuario traida de moodle
 * @param {UserDataSchema} localUserData datos del usuario
 */
export const postUserData = async (userId, localUserData = {}) => {
  if (!userId) {
    throw new Error('Es necesaria una id')
  }
  // eslint-disable-next-line no-param-reassign
  localUserData = { ...{}, ...userDataSchema, ...localUserData }
  localUserData.moodle_id = userId
  const normalizedData = normalizeUserData(localUserData)
  await api.post('users', normalizedData)
  return getUserById(userId)
}

/**
 *
 * @param {userDataSchema} userData
 */
export const putUserData = async userData => {
  delete userData.trivias
  const normalizedData = normalizeUserData(userData)
  await api.put(`users/${userData.id}`, normalizedData)
}
