import Vue from 'vue'

/** El "layout" */
import App from './App.vue'

import './bootstrap'

/** Módulos básicos */
import router from './router'
import store from './store'

/** Plugins */
import './registerServiceWorker'
import './font-awesome'
// import './carrousel'

/** Estilos */
import './assets/sass/app.scss'
import './assets/sass/global.scss'
import './assets/sass/transitions.scss'
import Collapse from './components/Collapse.vue'

Vue.config.productionTip = false

Vue.component('BCollapse', Collapse)

window.$app = new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
