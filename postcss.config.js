const isDev = process.env.NODE_ENV === 'production'
module.exports = () => ({
  plugins: {
    'postcss-import': {},
    'postcss-url': {},
    'postcss-color-function': {},
    // Solo habilitar si se tiene que soportar ie11
    // 'postcss-object-fit-images': {},
    // SEE: https://preset-env.cssdb.org/
    'postcss-preset-env': {
      cascade: false,
      grid: true,
      stage: 0,
      // NOTE: https://github.com/postcss/autoprefixer#does-autoprefixer-polyfill-grid-layout-for-ie
      autoprefixer: {
        grid: true,
      },
    },
    'postcss-reporter': { clearReportedMessages: true },
    cssnano: isDev
      ? false
      : {
          preset: 'default',
          discardComments: { removeAll: true },
          zindex: 100,
        },
  },
  order: 'cssnanoLast',
})
